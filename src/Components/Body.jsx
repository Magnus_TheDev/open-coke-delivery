import React, {useContext} from "react";

import {UserContext} from "../providers/UserProvider";
import Nav from './Nav';
import SectionArticle from "./SectionArticle";
import Footer from "./Footer";
import ProfilePage from "./ProfilePage";
import Home from "./Home";
import DealerShop from "./DealerShop";
import {Router} from "@reach/router";
import Cart from './Cart';
import DealerAdd from "./DealerAdd";
import Command from './Command';
import CommandList from './CommandList';
import ProduitAdd from "./ProduitAdd";
import ManageDealer from "./ManageDealer";

function Body() {
    const user = useContext(UserContext);
    return (
        <body className="bg-white text-gray-600 work-sans leading-normal text-base tracking-normal">
            <Nav />
                <Router>
                    <ProfilePage path="/profile"/>
                    <SectionArticle path="/"/>
                    <DealerShop path="/dealer/:id"/>
                    <Cart path={"/cart"}/>
                    <DealerAdd path="/dealer/add"/>
                    <Command path={"/command"}/>
                    <CommandList path={"/command/:id"}/>
                    <ProduitAdd path={"/produit/add"}/>
                    <ManageDealer path={"/dealer/manage"}/>
                </Router>
            <Footer />
        </body>
    );
}

export default Body;