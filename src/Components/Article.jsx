import React, {Component, useContext} from 'react';

import {firestore} from '../firebase';
import {Link} from '@reach/router';
import LazyLoad from 'react-lazy-load';

class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    firestore.collection('dealer').get().then(querySnapshot => {
      const data = querySnapshot.docs.map(doc => {
        let docu = doc.data();
        docu.id = doc.id;
        return docu;
      });
      this.setState({data: data});
    });
  }

  render() {
    return (
        <>
          {this.state.data.map(el => (
              <div className="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col"
                   key={el.id}>
                <Link to={"/dealer/"+el.id}>
                  <LazyLoad>
                  <img className="hover:grow hover:shadow-lg object-fill h-48 w-48 w-full"
                       src={el.image}/>
                  </LazyLoad>
                  <div className="pt-3 flex items-center justify-between">
                    <p className="">{el.name}</p>
                  </div>
                </Link>
              </div>
          ))}
        </>
    );
  }

}

export default Article;