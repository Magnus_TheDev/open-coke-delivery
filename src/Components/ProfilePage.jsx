import React, { useContext } from "react";
import { UserContext } from "../providers/UserProvider";
import {Link, navigate} from '@reach/router';
import {auth} from "../firebase";
const ProfilePage = () => {
  const user = useContext(UserContext);
  const {photoURL, displayName, email} = user;

  const logOut  = event => {
      auth.signOut();
      navigate('/');
    }

  return (
    <div className = "mx-auto w-11/12 md:w-2/4 py-8 px-4 md:px-8">
      <div className="flex border flex-col items-center md:flex-row md:items-start border-blue-400 px-3 py-4">
        <div
          style={{
            background: `url(${photoURL || 'https://res.cloudinary.com/dqcsk8rsc/image/upload/v1577268053/avatar-1-bitmoji_upgwhc.png'})  no-repeat center center`,
            backgroundSize: "cover",
            height: "200px",
            width: "200px"
          }}
          className="border border-blue-300"
        ></div>
        <div className = "md:pl-4">
        <h2 className = "text-2xl font-semibold">{displayName}</h2>
        <h3 className = "italic">{email}</h3>
        </div>
      </div>
        <br/>
        <div className="flex justify-between">
            <button className = "bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" onClick = {() => {logOut()}}>Sign out</button>
            <Link className ="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" to="/">Return Home</Link>
        </div>

    </div>
  ) 
};

export default ProfilePage;

