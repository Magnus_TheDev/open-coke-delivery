import React from 'react';
import '../Styles/components.css';
import {FirstTime} from '../providers/ShoppingCartProvider';
import {firestore} from '../firebase';
import {Link} from '@reach/router';

export default class Command extends React.Component {
  constructor(props) {
    super(props);
    FirstTime();
    this.state = {
      data: [],
      user: JSON.parse(localStorage.getItem('user')),
    };
    this.getCommand();
  }

  getCommand() {
    firestore.collection('command').
        where('user', '==', this.state.user.uid). where('status', 'in', [0,1,2]).get().
        then(querySnapshot => {
          const data = querySnapshot.docs.map(doc => {
            let docu = doc.data();
            docu.id = doc.id;
            return docu;
          });
          this.setState({data: data});
        });
  }

  render() {
    return (
        <>
          <section className="bg-white py-8">
            <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
              {this.state.data.map(el => (
                  <div className="max-w-sm rounded overflow-hidden shadow-lg">
                    <div className="px-6 py-4">
                      <div className="font-bold text-xl mb-2"> {el.id}
                      </div>
                      <p className="text-gray-700 text-base">
                      </p>
                    </div>
                    <div className="px-6 py-4">
                      <Link to={'/command/'+el.id}
                          class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Information
                      </Link>
                    </div>
                  </div>

              ))}
            </div>
          </section>
        </>
    );
  }
};


