import React, {Component} from 'react';

export default function(ComposedComponent) {
  class NetworkDetector extends Component {
    state = {
      isDisconnected: false,
    };

    componentDidMount() {
      this.handleConnectionChange();
      window.addEventListener('online', this.handleConnectionChange);
      window.addEventListener('offline', this.handleConnectionChange);
    }

    componentWillUnmount() {
      window.removeEventListener('online', this.handleConnectionChange);
      window.removeEventListener('offline', this.handleConnectionChange);
    }

    handleConnectionChange = () => {
      const condition = navigator.onLine ? 'online' : 'offline';
      if (condition === 'online') {
        const webPing = setInterval(
            () => {
              fetch('//google.com', {
                mode: 'no-cors',
              }).then(() => {
                this.setState({isDisconnected: false}, () => {
                  return clearInterval(webPing);
                });
              }).catch(() => this.setState({isDisconnected: true}));
            }, 2000);
        return;
      }

      return this.setState({isDisconnected: true});
    };

    render() {
      const {isDisconnected} = this.state;
      return (

          <div>
            {isDisconnected && (
                <div
                    className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                    role="alert">
                  <strong className="font-bold">Internet Is
                    Disconnected</strong>
                  <span
                      className="block sm:inline"> Please connect to a network</span>
                </div>

            )
            }
            <ComposedComponent {...this.props} />
          </div>
      );
    }
  }

  return NetworkDetector;
}