import React, {Component} from 'react';

import Application from "./Components/Application";
import UserProvider from "./providers/UserProvider";
import NetworkDetector from './providers/NetworkDetector';
import * as firebase from "firebase";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        const messaging = firebase.messaging();
        messaging.requestPermission().then(()=>{
            return messaging.getToken()
        }).then((token)=>{
            localStorage.setItem('token',token)
            console.log('Token :' + token)
        }).catch((error)=>{
            console.log(error);
        })
    }

    render(){
    return (
        <UserProvider>
          <Application />
        </UserProvider>
    );
  }
}

export default NetworkDetector(App);
